var MyUtils = {
  listAllChannels: (channelsList, callback) => {
    console.log(" - Channels available -");
    let string = "";
    for (let i = 0; i < channelsList.length; i++) {
      string += channelsList[i] + "\n";
    }
    console.log(string);
    callback();
  },

  listChannel: (channel, clientsList, callback) => {
    console.log(" - Users in channel " + channel + " -");
    let users = clientsList.getSubscribers(channel);
    let string = "";
    for (let i = 0; i < users.length; i++) {
      string += users[i] + "\n";
    }
    console.log(string);
    callback();
  },

  sendMessage: (user, message, clientsList, callback) => {
    let socket = clientsList.getSubscriberDetail(user)[0].id_socket;
    clientsList.send(socket, message);
    if (callback != null) callback();
  },

  sendAllMessage: (channel, message, clientsList, callback) => {
    let users = clientsList.getSubscribers(channel);
    for (let i = 0; i < users.length; i++) {
      MyUtils.sendMessage(users[i], message, clientsList, null);
    }
    callback();
  },

  userSendAllMessage: (channel, message, clientsList, user, callback) => {
    let users = clientsList.getSubscribers(channel);
    for (let i = 0; i < users.length; i++) {
      if (user != users[i]) {
        MyUtils.sendMessage(users[i], message, clientsList, null);
      }
    }
    callback();
  },


};

module.exports = MyUtils;

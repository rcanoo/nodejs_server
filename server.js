var net = require('net');
var ClientsList = require('./ClientsList.js');
var Utils = require('./myutils.js');

//////////////////
var PORT = 43234;
var ClientsList;

main();

function main() {
  ClientsList = new ClientsList();

  initServer(ClientsList);

  var consoleInput = function() {
    input(">> ", function(data) {
      var parts = data.trim().split(' ');
      if (parts[0]) parts[0] = parts[0].toUpperCase();

      switch (true) {
        case (parts[0] == "Q"):
          process.exit(0);
          break;
        case (parts[0] == "LIST"):
          //Hint: use ClientsList.getChannels(); and  ClientsList.getSubscribersDetail();
					if (parts.length == 2 && parts[1].toUpperCase() == "CHANNELS") {
	          Utils.listAllChannels(ClientsList.getChannels(), consoleInput);
					} else if (parts.length == 3 && parts[1].toUpperCase() == "CHANNEL") {
            let channelName = parts[2];
            Utils.listChannel(channelName, ClientsList, consoleInput);
          } else {
						consoleInput();
					}
          break;
        case (parts[0] == "SEND"):
          if (parts.length > 2) {
            let user = parts[1];
            let message = "Server: ";
            for (let i = 2; i < parts.length; i++) message += parts[i] + " ";
            Utils.sendMessage(user, message, ClientsList, consoleInput);
          } else {
            consoleInput();
          }
          break;
        case (parts[0] == "SENDALL"):
          if (parts.length > 2) {
            let channelName = parts[1];
            let message = "Server: ";
            for (let i = 2; i < parts.length; i++) message += parts[i] + " ";
            Utils.sendAllMessage(channelName, message, ClientsList, consoleInput);
          } else {
            consoleInput();
          }
          break;
        case (parts[0] == "WHEREIS"):
          if (parts[1]) {
            var details = clientsList.getSubscriberDetail(parts[1]);
            console.log(JSON.stringify(details));
          }
          setTimeout(consoleInput, 300);
          break;
        default:
          console.log("Incorrect command");
          consoleInput();
          break;
      }

    });
  };

  console.log("----------------------------")
  console.log("LIST - List regsitered CHANNELS and its sockets and stats");
  console.log("LIST [CHANNEL] - List regsitered subscribers in the CHANNEL");
  console.log("WHEREIS SUBSCRIBER - List CHANNELS and sockets of subscriber SUBSCRIBER");
  console.log("SEND [SUBSCRIBER] [MESSAGE] - Send MESSAGE to the SUBSCRIBER client identified")
  console.log("SENDALL [CHANNEL] [MESSAGE] - Send MESSAGE to all subscribers in the CHANNEL")
  console.log("----------------------------")
  console.log("Q - Exit");
  console.log("----------------------------")

  consoleInput();
}


function initServer(clientsList) {
  net.createServer(function(sock) {
    sock.id = Math.floor(Math.random() * 100000);
    console.log('CONNECTION RECEIVED: ' + sock.remoteAddress + ':' + sock.remotePort);
    sock.on('data', function(data) {
      var _self = this;
      console.log('DATA ' + sock.remoteAddress + ': ' + data);
      data.toString('utf-8').split('\n').forEach(function(message) {
        parseData(clientsList, message, _self);
      });
    });
    sock.on('close', function(data) {
      console.log('Connection closed for socket ' + sock.id);
      clientsList.removeSocket(sock.id);
    });
    sock.on('error', function(err) {
      console.log("Socket error " + sock.id, err.stack);
      clientsList.removeSocket(sock.id);
    });
    clientsList.addClient(null, sock, sock.id);
  }).listen(PORT);
  console.log('Server listening on port ' + PORT);
}

function parseData(clientsList, message, clientSocked) {
  //console.log("Parsing message: " + message + "/n");
	let data = message.trim().split(' ');
	if (data[0] == "REG" && data.length == 3) {
		let channel = data[1];
		let user = data[2];
		clientsList.setChannelToClient(clientSocked.id, user, channel);
		console.log("Registering new user '" +  user +"' to channel '" + channel + "'");
	}

  if (data[0] == "MSGALL" && data.length > 1) {
    let msg = "";
    for (let i = 1; i < data.length; i++) msg += data[i] + " ";
    console.log(msg);
    let user = clientsList.getSubscriberId(clientSocked.id);
    let channel = clientsList.getSubscriberDetail(user)[0].channel;
    Utils.userSendAllMessage(channel, msg, clientsList, user, ()=>{

    });
  }

  if (data[0] == "MSG" && data.length > 2) {
    let msg = "";
    let user = data[1];
    for (let i = 2; i < data.length; i++) msg += data[i] + " ";
    console.log("[To " + user + "] " + msg);
    Utils.sendMessage(user, msg, clientsList, null);
  }

  //console.log(">> ");
}


function input(question, callback) {
  var stdin = process.stdin,
    stdout = process.stdout;

  stdin.resume();
  stdout.write(question);

  stdin.once('data', function(data) {
    data = data.toString().trim();
    callback(data);
  });
}
